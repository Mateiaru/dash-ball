package org.matapps.android.dashball;

import android.graphics.Canvas;
import android.graphics.Path;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SpikeManager {
	
    public static final int TOP = 0;
    public static final int BOTTOM = 1;
    private int spikeWidth, spikeHeight, screenWidth, screenHeight;
    private int lastSpike = 0, lastImage = 0, currentImage = 0;
    private List<Spike> spikes = new ArrayList<>();
    private int frames = 0;
    private float speed = 5;
    private Random random;
    // Animations used to make game harder at some points
    public Animation anim0, anim1, anim2;

    public Spike[] update(boolean keepMinimumSpeed, boolean paused) {
        if(!paused) {
            frames++;
            if (lastSpike == 0) { // Check if the game has just started
                spikes.add(new Spike(getRandomLocation(), frames)); // Make a spike to begin with
                lastSpike = 1; // Update lastSpike
            }
            if (frames - lastSpike > 70 - (speed > 13 ? 13 : speed) * 4 && (frames < 4700 || frames > 4900)) { // Spawn spikes based on speed
                this.spikes.add(new Spike(getRandomLocation(), frames));
                lastSpike = frames;
            }
            speed = 5 + ((float)frames) / 300.0f; // Speed will increase by 1 for every 5 seconds
            if (speed > 13) speed = (frames < 4800 ? 13 : 16); // Limit speed to 13 / 16
            if (keepMinimumSpeed) speed = 5; // In main menu speed shouldn't increase
        }
        // Send all the spikes for processing / rendering
        return spikes.toArray(new Spike[this.spikes.size()]);
    }
    // Resets everything required for a new game
    public void restart() {
        lastSpike = 0;
        frames = 0;
        currentImage = 0;
        spikes = new ArrayList<>();
    }
    // Initialises everything needed by this object
    public void init(int screenWidth, int screenHeight, int spikeWidth, int spikeHeight) {
        random = new Random();
        this.screenWidth = screenWidth; // Set some stuff needed
        this.screenHeight = screenHeight; // by the spike generator
        this.spikeWidth = spikeWidth;
        this.spikeHeight = spikeHeight;
    }
    // Uses to generate TOP or BOTTOM
    private int getRandomLocation() {
        return (Math.random() > 0.5 ? BOTTOM : TOP);
    }

    public class Spike {

        private final int frame, location;
        @SuppressWarnings("unused")
        private Spike() {frame = 0; location = 0;} // Private constructor used to remove errors / warnings for unset values

        // Spike constructor
        public Spike(int location, int frame) {
            this.frame = frame; // Frame used to calculate position
            this.location = location; // Top or bottom
        }

        public boolean shouldDespawn() {
            // Calculate the position of the spike
            int pixelsPassed = (int)(((float)(frames - frame)) / 60 * spikeWidth * speed - spikeWidth);
            // The position will be number of seconds * spike width * game speed
            // True if the position will be out of the screen and should delete the spike
            return pixelsPassed > screenWidth;
        }

        // If the ball should die in this spike
        public boolean shouldDie(int location, boolean isJumping) {
            // Make sure we can die
            if (location != this.location || isJumping) return false;
            int pixelsPassed = (int)(((float)(frames - frame)) / 60 * spikeWidth * speed); // Calculate the position of the spike
			int ballEnd   = screenWidth / 5 + spikeWidth / 2;
			int ballStart = screenWidth / 5 - spikeWidth / 2;
            return (screenWidth - pixelsPassed < ballEnd && screenWidth - pixelsPassed + spikeWidth > ballStart);
        }

        // Calculate the spike's rendering
        public Path getPath() {
            Path path = new Path();
            int pixelsPassed = (int)(((float)(frames - frame)) / 60 * spikeWidth * speed); // Calculate the position of the spike
            if (location == TOP) {
                path.moveTo(screenWidth - pixelsPassed, 30);
                path.lineTo(screenWidth - pixelsPassed + spikeWidth, 30);
                path.lineTo(screenWidth - pixelsPassed + spikeWidth / 2, spikeHeight + 30);
                path.lineTo(screenWidth - pixelsPassed, 30);
            } else {
                path.moveTo(screenWidth - pixelsPassed, screenHeight - 30);
                path.lineTo(screenWidth - pixelsPassed + spikeWidth, screenHeight - 30);
                path.lineTo(screenWidth - pixelsPassed + spikeWidth / 2, screenHeight - (spikeHeight + 30));
                path.lineTo(screenWidth - pixelsPassed, screenHeight - 30);
            }
            path.close();
            return path;
        }

    }
    public void removeSpike(Spike spike) {
        spikes.remove(spike);
    }
    public int getScore() {
        return (int) (frames / 10.0f * (speed / 3.0f));
	}
    public int getFrames() {
        return frames;
    }
	public boolean isTryhardMode() {
		return frames >= 4800;
	}
    public void drawImagesIfNeeded(Canvas canvas) {
        // Only draw images after 100 seconds of gameplay, when in tryhard mode
        if (frames < 6000) return;
        // Draw an image every 15 seconds
        if (lastImage + 900 < frames) {
            currentImage = randomImage();
            lastImage = frames;
            switch (currentImage) {
                case 0: anim0.restart(); break;
                case 1: anim1.restart(); break;
                case 2: anim2.restart(); break;
            }
        }
        // Draw them for 2 seconds
        if (lastImage + 120 > frames) drawImage(canvas);
    }
    // Draw image based on currentImage
    private void drawImage(Canvas canvas) {
        switch (currentImage) {
            case 0: anim0.drawImage(canvas, screenWidth - anim0.getWidth(), 0); break;
            case 1: anim1.drawImage(canvas, screenWidth - anim1.getWidth(), 0); break;
            case 2: anim2.drawImage(canvas, screenWidth - anim2.getWidth(), screenHeight - anim2.getHeight()); break;
        }
    }
    // Synchronized random to prevent the same value to be given
    private synchronized int randomImage() {
        return random.nextInt(3);
    }
    // Releases all Animations
    public void releaseAnimations() {
        anim0.release();
        anim1.release();
        anim2.release();
    }
}

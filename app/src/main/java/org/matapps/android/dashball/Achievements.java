package org.matapps.android.dashball;

import java.util.ArrayList;
import java.util.List;

public class Achievements {
    // Make un-initialise-able
    private Achievements() {}
    // Private array
    private static final Achievement[] achievementsArray;
    // Returns an array of all achievements
    public static Achievement[] asArray() {
        return achievementsArray;
    }
    // Checks all achievements
    // Returns the list of completed achievements in this run
    public Achievement[] checkAndCompleteAll(SpikeManager spikeManager) {
        List<Achievement> achievements = new ArrayList<>();
        for (Achievement achievement : asArray()) {
            if (!achievement.isCompleted() && achievement.canComplete(spikeManager)) {
                achievement.complete();
                achievements.add(achievement);
            }
        }
        return achievements.toArray(new Achievement[achievements.size()]);
    }
    // All achievements
    // TODO: Add all achievements
    // Initialise the achievements
    static {
        // TODO: Initialise the achievements
        achievementsArray = new Achievement[]{};
    }
}

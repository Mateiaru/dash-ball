package org.matapps.android.dashball;

public class TimeClock {
    // Private values required for the clock
    private long start = 0, timeElapsed = 0;
    // By default it is not started so it is in paused state
    private boolean paused = true;
    // Starts or resumes the clock
    public void start() {
        if (!paused) return;
        start = System.currentTimeMillis();
        paused = false;
    }
    // Pauses the clock, if it is already paused or not started does nothing
    public void pause() {
        if (paused) return;
        timeElapsed += System.currentTimeMillis() - start;
        paused = true;
    }
    // Gets how much time has elapsed since the first start, minus the time when paused
    // Still works if it is paused or not
    public long getTimeElapsed() {
        return (paused ? timeElapsed : timeElapsed + System.currentTimeMillis() - start);
    }
    // Used so that no re-initialisation is required
    // Not that clock isn't restarted, start has to be called again
    public void reset() {
        paused = true;
        timeElapsed = 0; start = 0;
    }
}

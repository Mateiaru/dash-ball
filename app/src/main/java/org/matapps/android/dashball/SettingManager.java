package org.matapps.android.dashball;

import android.content.Context;
import android.content.SharedPreferences;

public class SettingManager {
    // Only used by loadSettings so must be private
    private SettingManager() {}
    // File name
    private static final String SHARED_PREFS = "settings";
    // Types of preferences
    private static final String HIGH_SCORE = "high_score";
    private static final String TIME = "time_played";
    private static final String SFX = "soundSfx";
    private static final String MUSIC = "soundMusic";
    // Private field used by achievements
    private Context context = null;
    // Static methods - save and load
    public static void saveSettings(Context context, SettingManager manager) {
        SharedPreferences.Editor preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).edit();
        preferences.putInt(HIGH_SCORE, manager.high_score);
        preferences.putLong(TIME, manager.timePlayed);
        preferences.putBoolean(SFX, manager.sfx);
        preferences.putBoolean(MUSIC, manager.music);
        preferences.apply();
    }
    public static SettingManager loadSettings(Context context) {
        SettingManager manager = new SettingManager();
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        manager.high_score = preferences.getInt(HIGH_SCORE, 0);
        manager.timePlayed = preferences.getLong(TIME, 0);
        manager.sfx  = preferences.getBoolean(SFX, true);
        manager.music = preferences.getBoolean(MUSIC, true);
        manager.context = context;
        return manager;
    }
    // Object fields
    private int high_score;
    private long timePlayed;
    private boolean sfx, music;
    // Getters
    public int getHighScore() {
        return high_score;
    }
    public long getTimePlayed() {
        return timePlayed;
    }
    public boolean isSfxEnabled() {
        return sfx;
    }
    public boolean isMusicEnabled() {
        return music;
    }
    // Setters
    public void setHighScore(int high_score) {
        this.high_score = high_score;
    }
    public void setTimePlayed(long timePlayed) {
        this.timePlayed = timePlayed;
    }
    public void setSfx(boolean sfx) {
        this.sfx = sfx;
    }
    public void setMusic(boolean music) {
        this.music = music;
    }
    // Resets all data
    public void reset() {
        high_score = 0;
        timePlayed = 0;
        for (Achievement achievement : Achievements.asArray()) {
            achievement.reset();
            saveAchievement(achievement);
        }
    }
    // Achievements stuff
    public void saveAchievement(Achievement achievement) {
        context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).edit()
                .putBoolean(achievement.getName(), achievement.isCompleted()).apply();
    }
    public Achievement loadAchievement(String name) {
        Achievement achievement = null;
        for (Achievement temp : Achievements.asArray()) {
            if (temp.getName().equals(name)) achievement = temp;
        }
        if (achievement != null && context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE).getBoolean(name, false))
            achievement.complete();
        return achievement;
    }
}

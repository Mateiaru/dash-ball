package org.matapps.android.dashball;

public class Status {
    private Status(){} // Make un-initialise-able
    // -------------------------
    public static final int MENU = 0;
    public static final int STATS = 1;
    public static final int OPTIONS = 2;
    public static final int EXIT = 3;
    public static final int GAME = 4;
    public static final int PAUSED = 5;
	public static final int LOST = 6;
    // -------------------------
}

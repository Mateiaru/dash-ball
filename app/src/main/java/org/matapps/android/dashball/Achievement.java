package org.matapps.android.dashball;

public abstract class Achievement {
    // Variables
    private boolean completed = false;
    private final String name;
    // Constructor
    public Achievement(String name) {
        this.name = name;
    }
    // Default methods
    public boolean isCompleted() {
        return completed;
    }
    public void complete() {
        completed = true;
    }
    public void reset() {
        completed = false;
    }
    public String getName() {
        return name;
    }
    // Abstract method
    public abstract boolean canComplete(SpikeManager spikeManager);
}

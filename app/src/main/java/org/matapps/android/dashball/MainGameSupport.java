package org.matapps.android.dashball;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.view.MotionEvent;

// MainGameSupport class is used to move a part of the code from the MainGame somewhere else
// as MainGame was getting way too big in size (almost 1000 lines of code)
public class MainGameSupport {
    static void onTouch(MotionEvent event, final MainGame c) {
        // User did not click / release, cancel this event
        if (!(event.getAction() == MotionEvent.ACTION_DOWN ||
                event.getAction() == MotionEvent.ACTION_UP)) return;
        // Get the coordinates of the event
        float x = event.getX();
        float y = event.getY();
        // If we have action down, check where the user clicked
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            c.B1 = isButtonTouched(x, y, c.getButtonX1(1), c.buttonY1, c.getButtonX2(1), c.buttonY2);
            c.B2 = isButtonTouched(x, y, c.getButtonX1(2), c.buttonY1, c.getButtonX2(2), c.buttonY2);
            c.B3 = isButtonTouched(x, y, c.getButtonX1(3), c.buttonY1, c.getButtonX2(3), c.buttonY2);
            c.B4 = isButtonTouched(x, y, c.getButtonX1(4), c.buttonY1, c.getButtonX2(4), c.buttonY2);
        }
        // Check where (in which view) is the user
        // Not that if it is in the game, actions should happen instantly, shouldn't wait for the user
        // To do action up
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (c.currentStatus== Status.GAME) {
                // If user has died disable controls
                if (c.isDead) return;
                // User is in game
                if (c.bgChanging + 1000 > SystemClock.uptimeMillis()) return;
                // Before jumping make sure pause button isn't pressed
                if (isButtonTouched(x, y, c.screenWidth - 30, 0, c.screenWidth, 30)) {
                    // If it is pressed, pause the game
                    c.gameMusic.pause();
                    c.clock.pause();
                    c.currentStatus = Status.PAUSED;
                    return;
                }
                // If we reached here it isn't pressed, so jump
                c.wantsToJump = true;
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            switch (c.currentStatus) {
                case Status.PAUSED:
                    if ((c.B1 || c.B2) && isButtonTouched(x, y, c.getButtonX1(1), c.buttonY1, c.getButtonX2(2), c.buttonY2)) {
                        playButtonSound(c);
                        // Seek the music to the right spot when resuming the game
                        // Otherwise multiple pauses & resumes may cause music problems
                        c.gameMusic.seekTo((int)(c.spikeManagerInstance.getFrames() / 60.0f * 1000.0f));
                        // There are 60 frames per second so divide by 60, but we need millis so multiply by 1000
                        // Play if we should
                        if (musicEnabled(c)) c.gameMusic.start();
                        // Timer
                        c.clock.start();
                        // Resume the game
                        c.currentStatus = Status.GAME;
                    } else if ((c.B3 || c.B4) && isButtonTouched(x, y, c.getButtonX1(3), c.buttonY1, c.getButtonX2(4), c.buttonY2)) {
                        playButtonSound(c);
                        c.spikeManagerInstance.restart();
                        // Update time played
                        c.settings.setTimePlayed(c.settings.getTimePlayed() + c.clock.getTimeElapsed());
                        // Go to menu
                        c.menuMusic.seekTo(0);
                        if (musicEnabled(c)) c.menuMusic.start();
                        c.currentStatus = Status.MENU;
                    }
                    break;
                case Status.LOST:
                    if ((c.B1 || c.B2) && isButtonTouched(x, y, c.getButtonX1(1), c.buttonY1, c.getButtonX2(2), c.buttonY2)) {
                        // Replay button clicked
                        playButtonSound(c);
                        c.wantsToJump = false;
                        c.gameMusic.seekTo(0);
                        if (musicEnabled(c))c. gameMusic.start();
                        c.bgChanging = SystemClock.uptimeMillis() - 500;
                        c.spikeManagerInstance.restart();
                        c.locationDelay = 0;
                        c.locationChanging = false;
                        c.location = SpikeManager.BOTTOM;
                        c.isDead = false;
                        c.currentStatus = Status.GAME;
                    } else if ((c.B3 || c.B4) && isButtonTouched(x, y, c.getButtonX1(3), c.buttonY1, c.getButtonX2(4), c.buttonY2)) {
                        // Menu button clicked
                        c.spikeManagerInstance.restart();
                        c.menuMusic.seekTo(0);
                        if (musicEnabled(c)) c.menuMusic.start();
                        c.currentStatus = Status.MENU;
                        playButtonSound(c);
                    }
                    break;
                case Status.MENU:
                    if (c.B1 && isButtonTouched(x, y, c.getButtonX1(1), c.buttonY1, c.getButtonX2(1), c.buttonY2)) {
                        // Play button clicked
                        // Reset all values for a new game
                        c.wantsToJump = false;
                        c.menuMusic.pause();
                        c.gameMusic.seekTo(0);
                        if (musicEnabled(c)) c.gameMusic.start();
                        c.locationDelay = 0;
                        c.locationChanging = false;
                        c.spikeManagerInstance.restart();
                        c.location = SpikeManager.BOTTOM;
                        c.isDead = false;
                        c.bgChanging = SystemClock.uptimeMillis();
                        c.currentStatus = Status.GAME;
                        // Start clock for the time played
                        c.clock.reset();
                        c.clock.start();
                        // Play button sound too
                        playButtonSound(c);
                    } else if (c.B2 && isButtonTouched(x, y, c.getButtonX1(2), c.buttonY1, c.getButtonX2(2), c.buttonY2)) {
                        // Stats button clicked
                        c.currentStatus = Status.STATS;
                        playButtonSound(c);
                    } else if (c.B3 && isButtonTouched(x, y, c.getButtonX1(3), c.buttonY1, c.getButtonX2(3), c.buttonY2)) {
                        // Options button clicked
                        c.currentStatus = Status.OPTIONS;
                        playButtonSound(c);
                    } else if (c.B4 && isButtonTouched(x, y, c.getButtonX1(4), c.buttonY1, c.getButtonX2(4), c.buttonY2)) {
                        // Exit button clicked
                        c.currentStatus = Status.EXIT;
                        playButtonSound(c);
                    }
                    break;
                case Status.STATS:
                    if (c.B4 && isButtonTouched(x, y, c.getButtonX1(4), c.buttonY1, c.getButtonX2(4), c.buttonY2)) {
                        playButtonSound(c);
                        c.currentStatus = Status.MENU;
                    }
                    break;
                case Status.OPTIONS:
                    if (c.B1 && isButtonTouched(x, y, c.getButtonX1(1), c.buttonY1, c.getButtonX2(1), c.buttonY2)) {
                        // SFX Button clicked
                        playButtonSound(c);
                        c.settings.setSfx(!(c.settings.isSfxEnabled()));
                        c.settingsChanged = true;
                    } else if (c.B2 && isButtonTouched(x, y, c.getButtonX1(2), c.buttonY1, c.getButtonX2(2), c.buttonY2)) {
                        // MUSIC Button clicked
                        playButtonSound(c);
                        c.settings.setMusic(!(c.settings.isMusicEnabled()));
                        if (c.settings.isMusicEnabled() && !c.menuMusic.isPlaying()) {
                            c.menuMusic.seekTo(0);
                            c.menuMusic.start();
                        }
                        if (!c.settings.isMusicEnabled() && c.menuMusic.isPlaying()) c.menuMusic.pause();
                        c.settingsChanged = true;
                    } else if (c.B3 && isButtonTouched(x, y, c.getButtonX1(3), c.buttonY1, c.getButtonX2(3), c.buttonY2)) {
                        // RESET Button clicked
                        playButtonSound(c);
                        new AlertDialog.Builder(c).setTitle("Are you sure?")
                                .setMessage("Are you sure you want to reset all your data (high score, stats, achievements)?")
                                .setNegativeButton("Cancel", null).setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                c.settings.reset();
                                c.settingsChanged = true;
                            }
                        }).show();
                    } else if (c.B4 && isButtonTouched(x, y, c.getButtonX1(4), c.buttonY1, c.getButtonX2(4), c.buttonY2)) {
                        // BACK Button clicked
                        playButtonSound(c);
                        c.currentStatus = Status.MENU;
                    }
                    break;
                case Status.EXIT:
                    if ((c.B1 || c.B2) && isButtonTouched(x, y, c.getButtonX1(1), c.buttonY1, c.getButtonX2(2), c.buttonY2)) {
                        // Yes button clicked
                        playButtonSound(c);
                        c.view.pause(); // Stop the view
                        c.finish();
                        c.currentStatus = Status.MENU;
                    } else if ((c.B3 || c.B4) && isButtonTouched(x, y, c.getButtonX1(3), c.buttonY1, c.getButtonX2(4), c.buttonY2)) {
                        // No button clicked
                        c.currentStatus = Status.MENU;
                        playButtonSound(c);
                    }
                    break;
            }
            // Reset the buttons
            c.B1 = false; c.B2 = false; c.B3 = false; c.B4 = false;
        }
    }
    // Check is X and Y are between specified values
    private static boolean isButtonTouched(float x, float y, int x1, int y1, int x2, int y2) {
        return (x > x1 && x < x2 && y > y1 && y < y2);
    }
    // Plays button sound if music is enabled
    private static void playButtonSound(MainGame mainGame) {
        if (mainGame.settings.isSfxEnabled()) mainGame.sounds.play(mainGame.buttonS, 1.0f, 1.0f, 1, 0, 1.0f);
    }
    // Tests is music is enabled
    private static boolean musicEnabled(MainGame mainGame) {
        return mainGame.settings.isMusicEnabled();
    }
    //
    // ***************************************************************
    //
    static void initPaints(MainGame c) {
        // Load the font needed by the game
        Typeface font = Typeface.createFromAsset(c.getAssets(), "font/main_font.ttf");
        // Title paint
        c.titlePaint = new Paint(); // Text paint
        c.titlePaint.setColor(Color.RED); // Red text
        c.titlePaint.setTextSize(c.screenHeight / 4); // Custom text size
        c.titlePaint.setTypeface(font); // Custom font
        // Paint used for paint or top / bottom margins
        c.spikeMargins = new Paint();
        c.spikeMargins.setStyle(Paint.Style.FILL);
        c.spikeMargins.setColor(Color.BLACK); // Black color
        // Button fill and stroke paints
        c.buttonP1 = new Paint();
        c.buttonP2 = new Paint();
        c.buttonP3 = new Paint();
        c.buttonP1.setStyle(Paint.Style.FILL);
        c.buttonP2.setStyle(Paint.Style.STROKE);
        c.buttonP3.setStyle(Paint.Style.FILL);
        // Set paints colors - green inside, yellow outside
        c.buttonP1.setColor(Color.GREEN);
        c.buttonP2.setColor(Color.YELLOW);
        // The button text paint
        c.buttonTextP = new Paint();
        c.buttonTextP.setColor(Color.BLACK); // Black color
        c.buttonTextP.setTextAlign(Paint.Align.CENTER); // Center alignment
        c.buttonTextP.setTypeface(font); // Custom font
        // The ball paints
        c.ballPaintIn = new Paint();
        c.ballPaintIn.setStyle(Paint.Style.FILL);
        c.ballPaintIn.setColor(Color.RED);
        c.ballPaintOut = new Paint();
        c.ballPaintOut.setStyle(Paint.Style.STROKE);
        c.ballPaintOut.setColor(Color.YELLOW);
        c.ballPaintOut.setStrokeWidth(c.spikeWidth / 10);
        // Find out text's size
        int size = 1;
        c.buttonTextP.setTextSize(size);
        while (true) {
            c.buttonTextP.setTextSize(size);
            // Options is the largest text and all texts should have same size
            // so use this one for calculation
            c.buttonTextP.getTextBounds("OPTIONS", 0, 7, c.textBounds);
            if (c.textBounds.width() > c.buttonWidth - 20) break;
            size++;
        }
        // Used for the small texts
        c.smallText = new Paint();
        c.smallText.setColor(Color.WHITE);
        c.smallText.setTypeface(font);
        c.smallText.setTextSize(27);
        // Used for stats
        c.mediumText = new Paint();
        c.mediumText.setColor(Color.YELLOW);
        c.mediumText.setTypeface(font);
        // Text size is initialized later in onCreate (after titleY because it is needed)
    }
}

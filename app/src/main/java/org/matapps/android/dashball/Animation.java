package org.matapps.android.dashball;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.SystemClock;

import java.io.IOException;

public class Animation {

    private Bitmap[] frames;
    private int frameLength, animationLength;
    private long start = 0;
    private final int width, height;

    // Disable default constructor
    @SuppressWarnings("unused")
    private Animation(){
        width = height = 0;
    }
    // Load all frames
    public Animation(Context context, String folder, int playFps, int width, int height) {
        // Save width and height
        this.width = width;
        this.height = height;
        // Make sure folder doesn't end with "/" as it will not work
        if (folder.endsWith("/")) folder = folder.substring(0, folder.length() - 1);
        // Calculate the frame length based on FPS
        frameLength = 1000 / playFps;
        // Get the assets from the context
        AssetManager assets = context.getAssets();
        // Load the frames
        int files = 0;
        try {
            files = assets.list(folder).length;
            animationLength = frameLength * files;
            frames = new Bitmap[files];
            for (int i = 0; i < files; i++) frames[i] = Bitmap.createScaledBitmap(
                    BitmapFactory.decodeStream(assets.open(folder + "/" + (i + 1) + ".jpg")), width, height, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void restart() {
        start = 0;
    }
    public void drawImage(Canvas canvas, float left, float top) {
        if (start == 0) start = SystemClock.uptimeMillis();
        long time = (SystemClock.uptimeMillis() - start) % animationLength;
        canvas.drawBitmap(getFrame(time), left, top, null);
    }
    private Bitmap getFrame(float time) {
        int frame = (int) (time / frameLength);
        return frames[frame > frames.length - 1 ? 0 : frame];
    }
    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
    // Releases all resources held by this object
    // Calling drawImage will no longer work as the bitmaps are recycled
    public void release() {
        for (Bitmap bitmap : frames) bitmap.recycle();
    }
}

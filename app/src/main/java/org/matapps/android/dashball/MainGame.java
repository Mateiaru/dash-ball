package org.matapps.android.dashball;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.IOException;

public class MainGame extends Activity implements View.OnTouchListener {

    // Used for testing / debugging
    private static boolean CAN_DIE = true;
    // Settings
    SettingManager settings;
    boolean settingsChanged = false;
    // The main surface view object
    MainGameView view;
    // Y values of some stuff
    int titleY, buttonY1, buttonY2;
    // Widths of different things set in onCreate
    int spikeWidth, spikeHeight, screenWidth, screenHeight, buttonWidth, spaceWidth;
    // Sound Pool for the short sounds like button click sound
    SoundPool sounds;
    // Menu and in-game music players
    MediaPlayer menuMusic, gameMusic;
    // Sample IDs  returned by the sound pool in onCreate
    int buttonS, deathS, thmS;
    // Used in the onTouch method
    boolean B1 = false, B2 = false, B3 = false, B4 = false; // B means button
    // Only one rect needed to be used by paint.getTextBounds, must not be re-created every time
    final Rect textBounds = new Rect();
    // All paints initialised by initPaints to increase performance
    Paint titlePaint, spikeMargins, buttonP1, buttonP2, buttonP3, buttonTextP, ballPaintIn, ballPaintOut, smallText, mediumText;
	// Used to control the background changing
	long bgChanging = 0;
    // Used to show location of the player (ball)
    int location = SpikeManager.BOTTOM; // Bottom default and will be set to button every game reset
    boolean locationChanging = false;
    long locationDelay = 0;
    // Used to make a delay before showing lost menu and ball death animation
    long death = 0;
    boolean isDead = false;
    // If this is true whenever the ball touches the ground jump and consume the boolean
    boolean wantsToJump = false;
    // The spike manager and status
    SpikeManager spikeManagerInstance;
    int currentStatus = Status.MENU;
    // Image used while in try-hard mode
    // thm = Try-Hard Mode
    private Bitmap thmImage, thmImage2;
    private float thmImageY1, thmImageX1, thmImageX2, thmImageY2;
    // Pause button bitmap and size
    private Bitmap pauseB;
    // Used for stats, in calculating time passed
    final TimeClock clock = new TimeClock();

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Create a SoundPool for the sounds, if we are in API 21 or more use the non-deprecated way
        if (Build.VERSION.SDK_INT >= 21) sounds = new SoundPool.Builder().setMaxStreams(3).build();
        else sounds = new SoundPool(3, AudioManager.STREAM_MUSIC, 100);
        // Load the sounds from /res/raw/
        // Music
        menuMusic = MediaPlayer.create(this, R.raw.reformat);
        gameMusic = MediaPlayer.create(this, R.raw.exit_the_premises);
        menuMusic.setLooping(true);
        gameMusic.setLooping(true);
        // SFX
        buttonS = sounds.load(this, R.raw.button, 1);
        deathS = sounds.load(this, R.raw.death, 1);
        thmS = sounds.load(this, R.raw.sanic_sound, 1);
        // Get the screen width and height, used in the game
        screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        screenHeight = getWindowManager().getDefaultDisplay().getHeight();
        spikeWidth = screenWidth / 10;
        // Calculation based on the math formula of the height in equilateral triangle
        spikeHeight = (int)(spikeWidth * Math.sqrt(3) / 2);
        // Buttons will have x-1 out of x button places on the screen
        buttonWidth = screenWidth / (5);
        // Spaces will have the rest
        spaceWidth = buttonWidth / (5);
        // Setup the thm (Try-Hard Mode) images
        // 1
        Bitmap temp = BitmapFactory.decodeResource(getResources(), R.raw.mlg);
        float height = screenHeight / 3;
        float width = height * temp.getWidth() / temp.getHeight();
        thmImage = Bitmap.createScaledBitmap(temp, (int) width, (int) height, false);
        thmImageY1 = screenHeight / 2 - height / 2;
        thmImageX1 = screenWidth - width - 40;
        // 2
        temp = BitmapFactory.decodeResource(getResources(), R.raw.sanic);
        height = screenHeight / 2;
        width = height * temp.getWidth() / temp.getHeight();
        thmImage2 = Bitmap.createScaledBitmap(temp, (int) width, (int) height, false);
        thmImageX2 = screenHeight / 3 - width / 3;
        thmImageY2 = screenHeight / 2 - height / 2;
        // Setup other images
        pauseB = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),
                R.raw.pause_button), 30, 30, false);
        // Initialize the Spike Manager
        spikeManagerInstance = new SpikeManager();
        spikeManagerInstance.init(screenWidth, screenHeight, spikeWidth, spikeHeight);
        // Setup the Animations
        BitmapFactory.Options options;
        int w, h;
        // First animation ( anim0 )
        options = new BitmapFactory.Options(); options.inJustDecodeBounds = true;
        try {BitmapFactory.decodeStream(getAssets().open("anim/mlg-frog/1.jpg"), null, options);}
        catch (IOException e) {e.printStackTrace();}
        w = screenWidth / 4;
        h = w * options.outHeight / options.outWidth;
        spikeManagerInstance.anim0 = new Animation(this, "anim/mlg-frog", 20, w, h);
        // Second animation ( anim1 )
        try {BitmapFactory.decodeStream(getAssets().open("anim/snoop-dog/1.jpg"), null, options);}
        catch (IOException e) {e.printStackTrace();}
        h = screenHeight;
        w = h * options.outWidth / options.outHeight;
        spikeManagerInstance.anim1 = new Animation(this, "anim/snoop-dog", 30, w, h);
        // Third animation ( anim2 )
        try {BitmapFactory.decodeStream(getAssets().open("anim/doge-zoom/1.jpg"), null, options);}
        catch (IOException e) {e.printStackTrace();}
        w = screenWidth / 4;
        h = w * options.outHeight / options.outWidth;
        spikeManagerInstance.anim2 = new Animation(this, "anim/doge-zoom", 30, w, h);
        // Create the paints
        MainGameSupport.initPaints(this);
        // Get some values which need:       the paints OR values that need the paints
        // Did that just for clarification
        titlePaint.getTextBounds("Dash Ball", 0, 9, textBounds);
        // Use absolute value just in case
        titleY = Math.abs(textBounds.height()) + 40 + spikeHeight;
        // Setup a paint's size that needs titleY
        // It should have up to 3 lines of text
        // From the screen height subtract the title Y position and the bar and spike height, also
        // subtract 40 more (totally 70) for 2 spaces, each 20. Finally subtract 40 more for the top and bottom
        // spacing, each is 20. The total is now 110
        mediumText.setTextSize((screenHeight - titleY - 110 - spikeHeight) / 3);
        // Menu button Y positions
        buttonY1 = titleY + 60;
        // Text bounds are the same from the above
        // so use it. Also add 20 to make them bigger
        buttonY2 = titleY + 80 + textBounds.height();
        // Load settings
        settings = SettingManager.loadSettings(this);
        // Create the content view
        view = new MainGameView(MainGame.this);
        // Set the content view
        setContentView(view);
        // And the on-touch listener
        view.setOnTouchListener(this);
        // Start the menu music
        if (settings.isMusicEnabled()) menuMusic.start();
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        MainGameSupport.onTouch(event, this);
        return true; // Consume the event
    }
    @Override
    protected void onPause() {
        super.onPause();
        view.pause(); // Pause the game
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Resume the states
        view.resume(); // Resume the game
    }
    // The main class of the game - the surface view
    public class MainGameView extends SurfaceView implements Runnable {

        private SurfaceHolder holder;
        private Thread thread;
        private boolean isRunning = true;

        public MainGameView(Context context) {
            super(context);
            // Just initialization
            holder = getHolder();
            thread = new Thread(this);
            thread.start();
        }

        @Override
        public void run() {
            // Main game thread
            while (isRunning) {
                long startTime = SystemClock.uptimeMillis(); // Get frame start time
                if (!holder.getSurface().isValid()) continue;
                // If settings have been edited, save them
                if (settingsChanged) {
                    SettingManager.saveSettings(MainGame.this, settings);
                    settingsChanged = false;
                }
				// If we are in fast mode draw red score
				if (currentStatus != Status.MENU && spikeManagerInstance.isTryhardMode())
					smallText.setColor(Color.RED); else smallText.setColor(Color.WHITE);
                Canvas canvas = holder.lockCanvas(); // Get the canvas to draw to
				int background = (int) (1000 - ((bgChanging + 1000) - SystemClock.uptimeMillis()));
				// Used to fade the background
				if (background < 500) canvas.drawRGB(0, 0, (500 - background) / 2);
				else if (background < 1000) canvas.drawRGB(0, 0, (background - 500) / 2);
                else canvas.drawColor(Color.BLUE); // Blue background
                // Draw the margins
                canvas.drawRect(0, 0, screenWidth, 30, spikeMargins); // Draw top / bottom margins
                canvas.drawRect(0, screenHeight - 30, screenWidth, screenHeight, spikeMargins);
                // Also fade the GUI
                int alpha = 255;
                if (background < 500) alpha = (500 - background) / 2;
                titlePaint.setAlpha(alpha);
                buttonP1.setAlpha(alpha);
                buttonP2.setAlpha(alpha);
                // Show the GUI based on status
                if (currentStatus == Status.MENU || background < 500) {
                    // Draw the text below the top spikes
                    canvas.drawText("Dash Ball", 20, titleY, titlePaint);
                    // Now draw the buttons: play, stats, options and exit
                    drawButton(getButtonX1(1), buttonY1, getButtonX2(1), buttonY2, "PLAY", B1, canvas);
                    drawButton(getButtonX1(2), buttonY1, getButtonX2(2), buttonY2, "STATS", B2, canvas);
                    drawButton(getButtonX1(3), buttonY1, getButtonX2(3), buttonY2, "OPTIONS", B3, canvas);
                    drawButton(getButtonX1(4), buttonY1, getButtonX2(4), buttonY2, "EXIT", B4, canvas);
                } else if (currentStatus == Status.EXIT) {
					// Draw the text below the top spikes
                    canvas.drawText("Exit game?", 20, titleY, titlePaint);
                    // Now draw the buttons: exit and cancel
                    drawButton(getButtonX1(1), buttonY1, getButtonX2(2), buttonY2, "QUIT", B1 || B2, canvas);
                    drawButton(getButtonX1(3), buttonY1, getButtonX2(4), buttonY2, "CANCEL", B3 || B4, canvas);
				} else if (currentStatus == Status.GAME) {
					// Fade in the ball and score text
                    int ballAlpha = 255;
					if (background < 1000 && background > 500) ballAlpha = (background - 500) / 2;
                    else if (background < 500) ballAlpha = 0;
					ballPaintIn.setAlpha(ballAlpha);
					ballPaintOut.setAlpha(ballAlpha);
					smallText.setAlpha(ballAlpha);
					// The score text
                    canvas.drawText("Score: " + spikeManagerInstance.getScore() +
                            (spikeManagerInstance.isTryhardMode() ? " TRYHARD MODE" : ""), 3, 26, smallText);
				} else if (currentStatus == Status.LOST) {
                    // The score text (yes, again)
                    canvas.drawText("Score: " + spikeManagerInstance.getScore() +
                            (spikeManagerInstance.isTryhardMode() ? " TRYHARD MODE" : ""), 3, 26, smallText);
                    // Draw the text below the top spikes
                    canvas.drawText("Game Over!", 20, titleY, titlePaint);
                    // Now draw the buttons: exit and resume
                    drawButton(getButtonX1(1), buttonY1, getButtonX2(2), buttonY2, "AGAIN", B1 || B2, canvas);
                    drawButton(getButtonX1(3), buttonY1, getButtonX2(4), buttonY2, "MENU", B3 || B4, canvas);
                } else if (currentStatus == Status.PAUSED) {
                    // The score text (yes, again)
                    canvas.drawText("Score: " + spikeManagerInstance.getScore() +
                            (spikeManagerInstance.isTryhardMode() ? " TRYHARD MODE" : ""), 3, 26, smallText);
                    // Draw the text below the top spikes
                    canvas.drawText("Game Paused", 20, titleY, titlePaint);
                    // Now draw the buttons: exit and resume
                    drawButton(getButtonX1(1), buttonY1, getButtonX2(2), buttonY2, "RESUME", B1 || B2, canvas);
                    drawButton(getButtonX1(3), buttonY1, getButtonX2(4), buttonY2, "QUIT", B3 || B4, canvas);
                } else if (currentStatus == Status.OPTIONS) {
                    // Draw the title
                    canvas.drawText("Options", 20, titleY, titlePaint);
                    // Draw the option buttons
                    drawOptionButton(getButtonX1(1), buttonY1, getButtonX2(1), buttonY2, "SFX",
                            new Pair<>("ON", "OFF"), B1, settings.isSfxEnabled(), canvas);
                    drawOptionButton(getButtonX1(2), buttonY1, getButtonX2(2), buttonY2, "MUSIC",
                            new Pair<>("ON", "OFF"), B2, settings.isMusicEnabled(), canvas);
                    drawOptionButton(getButtonX1(3), buttonY1, getButtonX2(3), buttonY2, "RESET",
                            new Pair<>("", "GAME"), B3, false, canvas);
                    drawButton(getButtonX1(4), buttonY1, getButtonX2(4), buttonY2, "BACK", B4, canvas);
                } else if (currentStatus == Status.STATS) {
                    // Draw the title
                    canvas.drawText("Stats", 20, titleY, titlePaint);
                    // Draw the back button
                    drawButton(getButtonX1(4), buttonY1, getButtonX2(4), buttonY2, "BACK", B4, canvas);
                    // Calculate the time played
                    // First get it in seconds
                    long timePlayed = settings.getTimePlayed() / 1000;
                    // Then get it in hours and minutes and seconds but < 60
                    long seconds = timePlayed;
                    while (seconds >= 60) seconds -= 60;
                    long minutes = timePlayed / 60;
                    while (minutes >= 60) minutes -= 60;
                    long hours = timePlayed / 3600;
                    // Draw the stats texts
                    drawText(20, titleY + 20, 20, "Highscore: " + settings.getHighScore() + "\n" +
                            "Time played: \n" + hours + " H " + minutes + " M " + seconds + " S", canvas);
                }
                // Draw top and bottom text
                if (currentStatus == Status.MENU || currentStatus == Status.EXIT ||
                        currentStatus == Status.STATS || currentStatus == Status.OPTIONS) {
                    canvas.drawText("Made by MATApps Org. No rights reserved", 3, 26, smallText);
                    canvas.drawText("Music by Kevin MacLeod - www.incompetech.com", 3, screenHeight - 3, smallText);
                }
                // Then the path for the spikes
                SpikeManager.Spike[] spikes = spikeManagerInstance.update(currentStatus!= Status.GAME,
					currentStatus == Status.PAUSED || currentStatus == Status.LOST || background < 1000 || isDead);
				// Update spike manager
                for (SpikeManager.Spike spike : spikes) {
                    // If the spike should despawn remove it and don't render
                    if (spike.shouldDespawn()) {
                        spikeManagerInstance.removeSpike(spike);
                        continue;
                    }
					// Check for collision
                    // If we have a collision, prepare for "game over" screen
                    if (!isDead && CAN_DIE)
                        if (currentStatus == Status.GAME && spike.shouldDie(location, locationChanging)) {
                            death = SystemClock.uptimeMillis();
                            isDead = true;
                            // Update the high score if should
                            if (settings.getHighScore() < spikeManagerInstance.getScore()) {
                                settings.setHighScore(spikeManagerInstance.getScore());
                            }
                            settings.setTimePlayed(settings.getTimePlayed() + clock.getTimeElapsed());
                            settingsChanged = true;
                            gameMusic.pause();
                            if (settings.isSfxEnabled()) sounds.play(deathS, 1.0f, 1.0f, 1, 0, 1.0f);
                        }
                    canvas.drawPath(spike.getPath(), spikeMargins); // Draw the spike
                }
                if (currentStatus == Status.GAME) {
                    // Draw images if needed
                    spikeManagerInstance.drawImagesIfNeeded(canvas);
                    // Draw the pause button
                    canvas.drawBitmap(pauseB, screenWidth - 30, 0, null);
                    // Play THM sound if required and allowed
                    if (spikeManagerInstance.getFrames() == 4800 && settings.isSfxEnabled())
                        sounds.play(thmS, 1.0f, 1.0f, 1, 0, 1.0f);
                    // Draw second thm image if needed
                    if (spikeManagerInstance.isTryhardMode() && spikeManagerInstance.getFrames() < 4920) {
                        canvas.drawBitmap(thmImage2, thmImageX2, thmImageY2, null);
                    }
                }
                // Jump if the user wants to (has pressed) and a few other conditions
                if (!isDead && currentStatus == Status.GAME && wantsToJump && !locationChanging) jump();
                // Check if the user has died before and death time has passed
                if (isDead && death + 300 < SystemClock.uptimeMillis()) {
                    currentStatus = Status.LOST;
                    isDead = false;
                }
                // MLG image if in tryhard mode
                if (spikeManagerInstance.isTryhardMode() && currentStatus == Status.GAME) {
                    canvas.drawBitmap(thmImage, thmImageX1, thmImageY1, null);
                }
                // Finally draw the ball
                if (currentStatus == Status.GAME || currentStatus == Status.PAUSED) if (background > 500) drawBall(0, canvas);
                holder.unlockCanvasAndPost(canvas); // Post updates
                // Calculate code run time, then sleep time required for 60 fps
                int sleepTime = (int) (16 - (SystemClock.uptimeMillis() - startTime));
                if (sleepTime < 2) sleep(2); else sleep(sleepTime); // Make sure system isn't overclocked even if it has lag
                // Frame finished
                // This code is ran 60 times per second
                // May decrease on older phones, causing lag
            }
        }
        // Stop the thread and join it
        // Also pause the music
        public void pause() {
            gameMusic.pause();
            menuMusic.pause();
            isRunning = false;
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // Re-create and re-start the thread
        // Also resume the music
        public void resume() {
            switch (currentStatus) {
                case Status.GAME:
                    if (settings.isMusicEnabled()) gameMusic.start();
                    break;
                case Status.MENU:
                case Status.EXIT:
                case Status.STATS:
                case Status.OPTIONS:
                    if (settings.isMusicEnabled()) menuMusic.start();
                    break;
            }
            isRunning = true;
            thread = new Thread(this);
            thread.start();
        }

    }
    // Used to sleep without having to handle exception and shorter use
    public void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    // On back key pressed
    @Override
    public void onBackPressed() {
        // Reset all buttons, without this reset there were appearing self-pressing buttons
        B1 = false;
        B2 = false;
        B3 = false;
        B4 = false;
        // Set the paused / exiting state based on location
        if (currentStatus == Status.GAME) {
            clock.pause();
            currentStatus = Status.PAUSED;
            gameMusic.pause();
        } else if (currentStatus == Status.EXIT) {
            view.pause();
            finish();
            currentStatus = Status.MENU;
        } else if (currentStatus == Status.PAUSED) {
            spikeManagerInstance.restart();
            menuMusic.seekTo(0);
            if (settings.isMusicEnabled()) menuMusic.start();
            currentStatus = Status.MENU;
        } else if (currentStatus == Status.LOST) {
            currentStatus = Status.MENU;
            spikeManagerInstance.restart();
            menuMusic.seekTo(0);
            if (settings.isMusicEnabled()) menuMusic.start();
        } else if (currentStatus == Status.STATS) {
            currentStatus = Status.MENU;
        } else currentStatus = Status.EXIT;
    }
    // Used to create buttons. The 4 ints are coordinates for the rectangle, string is button text,
    // boolean changes the texture based if it is clicked or not and the canvas is used to draw to
    private void drawButton(int left, int top, int right, int bottom, String text, boolean pressed, Canvas canvas) {
        // Change stroke based on press state, pressing increased width
        buttonP2.setStrokeWidth((pressed ? 12 : 3));
        // Draw the rectangles - both fill and stroke
        canvas.drawRect(left, top, right, bottom, buttonP1);
        canvas.drawRect(left, top, right, bottom, buttonP2);
        // Now the text
        // First get its bounds for vertical centering and setting size
        buttonTextP.getTextBounds(text, 0, text.length(), textBounds);
        // Then draw it to the canvas
        canvas.drawText(text, (right + left) / 2, (top + bottom) / 2 - textBounds.exactCenterY(), buttonTextP);
    }
    // Used to draw a 2-line activator button
    private void drawOptionButton(int left, int top, int right, int bottom, String text,
                                  Pair<String, String> secondLinePair, boolean pressed, boolean activated, Canvas canvas) {
        String secondLine = (activated ? secondLinePair.first : secondLinePair.second);
        // Change stroke based on press state, pressing increased width
        buttonP2.setStrokeWidth((pressed ? 12 : 3));
        // If the option is enabled, draw green button; red otherwise
        if (activated) buttonP3.setColor(Color.GREEN);
        else buttonP3.setColor(Color.RED);
        // Draw the rectangles - both fill and stroke
        canvas.drawRect(left, top, right, bottom, buttonP3);
        canvas.drawRect(left, top, right, bottom, buttonP2);
        // Now the text
        canvas.drawText(text, (right + left) / 2, (top + bottom) / 2 - 10, buttonTextP);
        canvas.drawText(secondLine, (right + left) / 2, (bottom - 10), buttonTextP);
    }
    // Used lines of text from top to bottom with a spacing, starting from the top - left point given
    private void drawText(int left, int top, int spacing, String text, Canvas canvas) {
        String[] texts = text.split("\n");
        for (String line : texts) {
            mediumText.getTextBounds(line, 0, line.length(), textBounds);
            top += Math.abs(textBounds.height());
            canvas.drawText(line, left, top, mediumText);
            top += spacing;
        }
    }
    // NOTE: on getButtonX1 and getButtonX2 spike counting starts
    // from 1 instead of the regular 0
    //
    // Get the button X1 position (left) based on its number
    int getButtonX1(int which) {
        return spaceWidth * which + buttonWidth * (which - 1);
    }
    // Get the button X2 position (right) based on its number
    int getButtonX2(int which) {
        return spaceWidth * which + buttonWidth * which;
    }
    // Ball drawer, which requires an int to choose texture
    // and a canvas to draw to
    private void drawBall(int texture, Canvas canvas) {
		int jumpTime = (spikeManagerInstance.isTryhardMode() ? 200 : 300);
        float radius = spikeWidth / 2;
        int strokeOutside = spikeWidth / 10;
        // If player has lost make the ball smaller and smaller
        if (isDead)
            radius = (int) (radius - radius * ((jumpTime - ((death + jumpTime) - SystemClock.uptimeMillis()))) / 100 - 1);
        // TODO: Textures
        //noinspection StatementWithEmptyBody
        if (texture == 0);
        // Ball is in mid-air
        if (locationChanging) {
            long time = SystemClock.uptimeMillis();
            int currentLocation = (int) (time - locationDelay);
            // Ball has landed
            // Takes 0.3 seconds a ball travel
			// 0.2 on fast mode
            if (currentLocation > jumpTime) {
                if (location == SpikeManager.TOP) location = SpikeManager.BOTTOM;
                else location = SpikeManager.TOP;
                locationChanging = false;
				// Trigger the else to draw
				drawBall(texture, canvas);
                return;
            }
            // Calculate ball position
            int minPossible = 33 +  spikeHeight / 2 + strokeOutside;
            int maxPossible = screenHeight - (33 +  spikeHeight / 2 + strokeOutside) - minPossible;
            int locationPos = currentLocation * maxPossible / jumpTime + minPossible;
            if (location == SpikeManager.BOTTOM)  locationPos = screenHeight - locationPos;
            canvas.drawCircle(screenWidth / 5, locationPos, radius, ballPaintIn);
            canvas.drawCircle(screenWidth / 5, locationPos, radius, ballPaintOut);
        } else {
            if (location == SpikeManager.TOP) {
                canvas.drawCircle(screenWidth / 5, 30 + spikeHeight / 2 + strokeOutside, radius, ballPaintIn);
                canvas.drawCircle(screenWidth / 5, 30 + spikeHeight / 2 + strokeOutside, radius, ballPaintOut);
            }
            else {
                canvas.drawCircle(screenWidth / 5, screenHeight - (30 + spikeHeight / 2 + strokeOutside), radius, ballPaintIn);
                canvas.drawCircle(screenWidth / 5, screenHeight - (30 + spikeHeight / 2 + strokeOutside), radius, ballPaintOut);
            }
        }
    }
    // Swaps the ball's location
    private void jump() {
        if (locationChanging) return;
        locationDelay = SystemClock.uptimeMillis();
        locationChanging = true;
        wantsToJump = false;
    }
    // Release the SoundPool on destroy
    @Override
    protected void onDestroy() {
        super.onDestroy();
        sounds.release();
        gameMusic.release();
        menuMusic.release();
        spikeManagerInstance.releaseAnimations();
    }
}
